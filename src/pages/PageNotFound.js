import React from 'react'
const PageNotFound = () => {

  return (
    <div>
    <h1 class="heading-not-found">404</h1>
<p class="not-found">Oops! Something went wrong. Return home.</p>
    </div>
  )
}

export default PageNotFound
